Overall goal

## 1. Create folder structure
## 2. Recontruct code for resuability and user readability, inline commenting
## 3. Document code
## 4. speed up code
## 5. package code




# 1

Folder structure
```
tsetse-fly-landmark-detection-seams--
                                    |_Data
                                    |_Training
                                    |_Testing
                                    |_Predict
                                    |_Example
```

# 2 
1. create predicting script


    The predict function must take in an image and do all the preprocessing and 
    feature extraction from the image that will be subsequently
    fed into the network, whereby the outputs are used to average and the result
    is returned as the output

    The inputs for this function should be **model**, **image** and the
    **centre** sampling position.
    Other inputs are set as defaults and are used in other auxilary 
    fucntions already developed in the raw project.
    
        predict(image) -> coordinate
        
        
2. create preprocessing script


   
    This script must process the files given to be used as inputs for the 
    training script.



2. create Training script

    The origional raw project contains functionality for training but it was
    conatained in the same script as the testing. The preprossessing where all
    the training data was created was done in a seperate script in which their 
    are multiple fucntions (too many) and they contain alot of hard coded specifics
    and are dependent on other 
    functions. The inputs need can be less complicated and more intuiative for 
    readablilty and usability. The outputs need to be optimal for use in other 
    functions and the preprocessing can be done as part of the training as 
    seperating these is not usefull for the user. 
    To proceed:
    
    The new project will contain a single function that takes in inputs; image 
    folder and csv containing image names and landmarks, The output will be a 
    model object, saved as a pickle file. 
    The training function will contain the functions used in the raw project 
    modified so that the inputs are clear and fucntions are modular. 
        - disentangle fucntions / modularise
        - create main training function
   
        training(model, images, label) -> forest object
        
3. Think about a better naming scheme, how should files be named?


4. Create testing script


5. Create example script


    The orginal raw project contained functionality for testing but the inputs
        - model, images, labels -> test metrics
    The testing script should take in a folder containing all the test images
    and a csv file with all the names and landmarks (same with training). The
    output should be dataframe file containing all the
    results (euclidean distances).
    
# 3. 

1. Document on gitlab in read me    
 


# 4. 


1. install Cuda for nvida grapghics cards and paralelise the jobs. 


    
# 5.
    
2. using PyPI standard and publish.
    
    

        
 