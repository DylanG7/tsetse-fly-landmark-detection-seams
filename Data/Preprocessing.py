
# INPUT: refined_data.txt
# OUTPUT: landmark_data.csv

import pandas as pd 
import os
import numpy as np

#directories
current = "./" #"/home/dylan/Projects/Tetse_proj/Dylan/"
C_data =  "C_data/"
refined_data = 'refined_data.txt'

####################### make a dataframe/csv from the refined_data.txt containing all landmarks #########################
# NOTE: not all the the images will be used since some where not labelled correctly and for this project we only used right wings
df = pd.read_fwf(current + refined_data,header=None)
which = lambda lst:list(np.where(lst)[0])
new = df.iloc[:,4].str.split(" ", n = 23, expand = True)
df= df.drop(labels=4, axis=1)
result = pd.concat([df, new], axis=1, sort=False)
        # column numbers were messed up, make them go according to range
result.columns = range(result.shape[1])
result.reset_index(inplace=True, drop=True)
text_data = result.copy() 

###################################### clean data  #######################################
# NOTE: all the images being used are in C_data. In the following code the unused images are removed from text_data
# and saved as 'new_textFile.csv'.
new_df = pd.DataFrame([])
for filename in os.listdir(C_data):
    for j in range(text_data.shape[0]):
        im_name = " ".join(text_data.loc[j,[0,1,2]])
        if filename == im_name:
            #print(text_data.iloc[[j]])
            new_df = new_df.append(text_data.iloc[[j]])

new_df = new_df.reset_index(drop=True)

############################# Format #############################################
# in the new_df, the name of an image is sectioned on different e.g collumns 'A002 -' | '20170126_195145' | '.jpg' .
# In the following collumn we make it one collumn e.g |'A002 - 20170126_195145.jpg'|

templist = []
for i in range(len(new_df)):
    imgname = " ".join(new_df.iloc[i,[0,1,2]])
    templist.append(imgname)
templist_df = pd.DataFrame(templist)
landmark_data = pd.concat([templist_df,new_df.iloc[:,3:]], axis=1)
landmark_data.columns = [i for i in range(len(landmark_data.columns))]
landmark_data.to_csv('landmark_data.csv')