## Automated landmark detection in Tsetse fly wing images

# Aim:
Automate the task of finding the pixel coordinates of specific feature
       points (landmarks).
       
A wing with all its landmarks annotated is shown bellow

![input 1](wingLandmark.png)
# Sceintific question to be adressed
Can we use morpholigical data to measure the simularity between two different
tsetse fly populations?

This meaasure will allow researchers to determine whether two tsetsefly 
populations are different enough to ensure there is no mixing between these two 
populations. This information can then help policy makers to take more informed
measures to eradricate tsetse flies in an area. 

# Model description:
the automation is achieved by training a random forest 
classifier (machine learning model) to identify landmark
pixels. 
                   
# Project design: The project is spit into 3 main functional parts.
1. Training: 
   The function of the script Training.py is to take in data and 
   labels which will be processed and used to train the random forest model. 
   There is 2 inputs for the function Training. 
   input 1: is a directory path to the folder containing all of the images to be 
   used for training.
   input 2:  is a csv file containing all the image names in the the first collumn
   and the labels in the proceding collumns. In this case the label are the 
   landmark coordinates.
   The output of this script is a random forest object which can be
   saved as a pickle file.
   This file can be opened using the pickle.load(), see documentation
   for more details on pickling https://docs.python.org/3/library/pickle.html

   an example of the csv file (input 2) is shown below

   ![input 2](lanmark_data_INPUTexample.PNG)
2. Testing;
   The Testing.py script uses model object, image directory and label csv as
   The input for this funtion; the same as the training script,
   but on a different set of data (not used in training). 
   The output is a dataframe containing the euclidean distance errors and the 
   amount of samples positively classified per an image.
    
3. prediction;
   The prediction script is to be used as a production script. It contains a 
   function that takes in an unseen image and outputs the predicted coordinate.

# Other documents
  The **preprocessing** file is used to process the data given for this project
  to be used as inputs in the above scripts. 
  The **example** folder contains an example script, showing how to use the functions
  
                
