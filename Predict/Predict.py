import pandas as pd 
import pickle
import numpy as np
import math
import statistics as stats 

from skimage.transform import integral_image
from skimage.feature import haar_like_feature
from skimage.feature import haar_like_feature_coord
from skimage.feature import draw_haar_like_feature

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import asarray

#######################################################directories############################################################
current = "./" #"/home/dylan/Projects/Tetse_proj/Dylan/"
main = "../"
C_data =  main + "Data/C_data/"

##############################################################################################################################


                 ############################## General fuctions to be used in landmarks() ###################################

def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


def get_window(PIL_image, sample_coord , Crop=50, thumbnail=5):
    x,y = sample_coord
    img_cropped = PIL_image.crop((x- Crop,y-Crop,x+Crop,y+Crop) )
    #plt.show(img_cropped)
    #plt.show()
    img_cropped.thumbnail([thumbnail,thumbnail])
    return img_cropped


feature_types = ['type-4' , 'type-2-x', 'type-2-y','type-3-x', 'type-3-y']

def extract_feature_image(img, feature_types, feature_coord=None):
    """Extract the haar feature for the current image"""
    ii = integral_image(img)
    features = []
    for i in range(len(feature_types)):
        features.extend(haar_like_feature(ii, 0, 0, ii.shape[0], ii.shape[1],
                             feature_types[i],
                             feature_coord=None))
    return features


               ################################### Landmarks (production function) #########################################


# This function outputs the predictied coordintes from a given image
# NOTE: std needs to be calculated from actual data, change this in future update!
def Predict(trained_ModelObject, PIL_image, center = 'Anywhere', std = 100,Nsamples = 10000 ):

    # In the future I would like to add functionality for uniformuly sampling the whole image/cropped window
    # in which case a center will not be given, Hence; if center != 'None': 
    if center != 'Anywhere': 

        Rmax= std +50

 
        

        a = np.random.uniform(0,1,Nsamples) * 2 * np.pi
        r = np.random.normal(0,Rmax,Nsamples)

        ## If you need it in Cartesian coordinates

        xwithin = r * np.cos(a)
        ywithin = r * np.sin(a)

        posLandmarks_Xcoords = []
        posLandmarks_Ycoords = []
        for i in range(len(xwithin)):
            # for each sample get the feature descriptors and feed into the model 
            sample = (center[0] + xwithin[i],center[1] + ywithin[i])
            features = extract_feature_image(rgb2gray(asarray(get_window(PIL_image,sample))), feature_types)
            s = pd.DataFrame([features])
            pred = trained_ModelObject.predict(s)
            # If the sample is 'pos' append it to a list
            if pred[0] == 'pos':
                posLandmarks_Xcoords.append(sample[0])
                posLandmarks_Ycoords.append(sample[1])
            # get the median x and y coordinate of the list and return this as final landmark prediction
        if posLandmarks_Xcoords == []:
            print("No landmark was found near {} ".format(center))
        else:  
            x_median = stats.median(posLandmarks_Xcoords)
            y_median = stats.median(posLandmarks_Ycoords)
            return(x_median, y_median)

def Predict_For_TestOutput(trained_ModelObject, PIL_image, center = 'Anywhere', std = 100,Nsamples = 10000 ):

    # In the future I would like to add functionality for uniformuly sampling the whole image/cropped window
    # in which case a center will not be given, Hence; if center != 'None': 
    if center != 'Anywhere': 

        Rmax= std + 50

        
        

        a = np.random.uniform(0,1,Nsamples) * 2 * np.pi
        r = np.random.normal(0,Rmax,Nsamples)

        ## If you need it in Cartesian coordinates

        xwithin = r * np.cos(a)
        ywithin = r * np.sin(a)

        posLandmarks_Xcoords = []
        posLandmarks_Ycoords = []
        count_pos = 0
        for i in range(len(xwithin)):
            # for each sample get the feature descriptors and feed into the model 
            sample = (center[0] + xwithin[i],center[1] + ywithin[i])
            features = extract_feature_image(rgb2gray(asarray(get_window(PIL_image,sample))), feature_types)
            s = pd.DataFrame([features])
            pred = trained_ModelObject.predict(s)
            # If the sample is 'pos' append it to a list
            if pred[0] == 'pos':
                count_pos += 1
                posLandmarks_Xcoords.append(sample[0])
                posLandmarks_Ycoords.append(sample[1])
            # get the median x and y coordinate of the list and return this as final landmark prediction
        if posLandmarks_Xcoords == []:
            print("No landmark was found near {} ".format(center))
        else:  
            x_median = stats.median(posLandmarks_Xcoords)
            y_median = stats.median(posLandmarks_Ycoords)
            return(count_pos,x_median, y_median)





