import sys
sys.path.insert(0, '../Predict/')
from Predict import *
from PIL import Image

import math
current = "./" 
main = "../"
import numpy as np


def Test(TrainedModel, Image_Folder, landmark_data ):
    # first calculate the average center as this will only need to be calc once
    x1landmark = landmark_data.iloc[:,1]
    y1landmark = landmark_data.iloc[:,2]
    x1landmark_mean, y1landmark_mean = np.mean(x1landmark), np.mean(y1landmark)
    center = (x1landmark_mean, y1landmark_mean)
    # create an empty list (distances) to append the distances errors to; abs(landmark - prediction)
    distances = []
    for i in range(len(landmark_data)):
        PIL_image = Image.open(Image_Folder + landmark_data.iloc[i,0])
        count_pos,x_pred,y_pred = Predict_For_TestOutput(TrainedModel, PIL_image, center)
        distances.append([count_pos ,landmark_data.iloc[i,0],math.sqrt((abs(landmark_data.iloc[i,1]- x_pred))**2
                     + (abs(landmark_data.iloc[i,2] - y_pred))**2 )])
        distances = pd.DataFrame(distances, columns = ['No._pos_samples_averaged','Image_name','distance' ])
    return(distances)











     
     
