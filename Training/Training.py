# TODO
# create input files, folder and pandas array containing names and landmarks 
# fix pix_sampling_training
# create training function


#import all modules
from PIL import Image
import pandas as pd
import sys
from time import time

### for ROC curve metrics
from sklearn import metrics

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#from dask import delayed

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score

from skimage.data import lfw_subset
from skimage.transform import integral_image
from skimage.feature import haar_like_feature
from skimage.feature import haar_like_feature_coord
from skimage.feature import draw_haar_like_feature

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numpy import asarray
import pickle

######################################## Directories ##########################################
current = "./" 
main = "../"
C_data =  main + "Data/C_data/"

####################################### Callable functions 1 ####################################
def pix_sampling_training(imgname, image, Xcoord_Landmark, Ycoord_Landmark, R=9,Rmax=300, NposPIX=100): #, imgname, i):

    coord_label_df = pd.DataFrame(columns=['label', 'coord', 'Image_name'])

    
    NnegPIX = 2*NposPIX

    a = np.random.uniform(0,1,NposPIX) * 2 * np.pi 
    r = R * np.sqrt(np.random.uniform(0,1,NposPIX)) 

    ## If you need it in Cartesian coordinates
    xwithin = r * np.cos(a)
    ywithin = r * np.sin(a)

    outer_radius =Rmax*Rmax
    inner_radius = R*R
    rho= np.sqrt(np.random.uniform(inner_radius, 
                             outer_radius, size=NnegPIX))

    theta=  np.random.uniform( 0, 2*np.pi, NnegPIX)
    xhoop = rho * np.cos(theta)
    yhoop = rho * np.sin(theta)

    for i in range(len(xwithin)):
        df2_within = pd.DataFrame({'label': ['pos'], 'coord': [(Xcoord_Landmark + 
                xwithin[i],Ycoord_Landmark + ywithin[i])], 'Image_name':[imgname]})
        coord_label_df = coord_label_df.append(df2_within)
        
    for i in range(len(xhoop)):
        df2_hoop = pd.DataFrame({'label': ['neg'], 'coord': [(Xcoord_Landmark + 
                xhoop[i], Ycoord_Landmark + yhoop[i])], 'Image_name': [imgname]})
        coord_label_df = coord_label_df.append(df2_hoop)

    return(coord_label_df)


def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray


def get_window(PIL_image, sample_coord , Crop=50, thumbnail=5):
    x,y = sample_coord
    img_cropped = PIL_image.crop((x- Crop,y-Crop,x+Crop,y+Crop) )
    #plt.show(img_cropped)
    #plt.show()
    img_cropped.thumbnail([thumbnail,thumbnail])
    return img_cropped


feature_types = ['type-4' , 'type-2-x', 'type-2-y','type-3-x', 'type-3-y']

def extract_feature_image(img, feature_types, feature_coord=None):
    """Extract the haar feature for the current image"""
    ii = integral_image(img)
    features = []
    for i in range(len(feature_types)):
        features.extend(haar_like_feature(ii, 0, 0, ii.shape[0], ii.shape[1],
                             feature_types[i],
                             feature_coord=None))
    return features

######################################### Callable functions 2 #############################################

def generate_sample_df(Image_Folder, DataFrame):
    df = pd.DataFrame(columns=['label', 'coord', 'Image_name'])
    for i in range(len(DataFrame)):
        imageName = DataFrame.iloc[i,0]
        landmark_Xcoord = DataFrame.iloc[i,1]
        landmark_Ycoord = DataFrame.iloc[i,2]
        image = Image.open(Image_Folder + imageName)
        df = df.append([pix_sampling_training(imageName, image, landmark_Xcoord, landmark_Ycoord, R=9,Rmax=300, NposPIX=100)])
        df.reset_index(drop=True)
    return(df)

# get_training_data uses the output of the function get_sample_df as an input
def generate_training_data(df,Image_Folder ):
    df_final = pd.DataFrame([])
    for i in range(len(df)):
        line = df.iloc[[i]]
        line = line.reset_index(drop=True)
        imgname = line.iloc[0,2]

        img = Image.open(Image_Folder+ imgname)
        
        window = get_window(img, line.iloc[0,1])

        features = extract_feature_image(rgb2gray(asarray(window))
                                         , feature_types)

        s = pd.DataFrame([features])

        data = line.join(s)

        df_final = df_final.append(data)
        df_final = df_final.reset_index(drop = True)
    return(df_final)    


############################################# example #######################################################

##########################################Initiate inputs####################################################
DataFrame = pd.read_csv(main + 'Data/landmark_data.csv')
DataFrame.drop(DataFrame.columns[[0]], axis=1, inplace=True)
DataFrame  = pd.concat([DataFrame.iloc[:,0],DataFrame.iloc[:,13:15]], axis=1)

##############################################################################################

def Training(Image_Folder, DataFrame, modelobject=RandomForestClassifier()):
    df = generate_sample_df(C_data, DataFrame)
    training_data = generate_training_data(df,C_data )
    X_train = training_data.drop(['label','coord','Image_name'],  axis=1)
    Y_train = training_data['label']
    modelobject.fit(X_train,Y_train)#
    return(modelobject)
